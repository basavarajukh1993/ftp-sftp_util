from unittest.mock import patch, mock_open, Mock

import pytest

from src.File import File


class TestFileUtil:
    util = File()

    @patch('builtins.open', new_callable=mock_open())
    def test_should_create_file_in_specified_file_path(self, mocked_open):
        mocked_open.write.return_value = True

        self.util.create_file("/tmp/test.json", "Test")

        mocked_open.assert_called_once_with("/tmp/test.json", "w")
        mocked_open.return_value.__enter__().write.assert_called_once_with("Test")
        assert mocked_open.return_value.__exit__.called

    @patch('builtins.open', new_callable=mock_open())
    def test_should_throw_error_if_unable_to_open_file(self, mocked_open):
        mocked_open.side_effect = Exception()
        with pytest.raises(Exception) as exception:
            self.util.create_file("new_data", "file_path")

    @patch("os.walk", return_value=[["", ["dir1"], ["file1", "file2"]]])
    @patch("os.remove", side_effect=[True, True])
    @patch("os.rmdir", side_effect=[True, True])
    def test_should_remove_all_files_from_tmp_directory(self, mocked_remove_dir, mocked_remove, mocked_walk):
        self.util.clear_tmp_directory('some_dir')

        assert mocked_remove_dir.call_count == 1
        assert mocked_remove.call_count == 2

    def test_should_return_file_name_excluding_path_details(self):
        file_name = self.util.get_file_name("/tmp/DVIM.xml")
        assert file_name == "DVIM.xml"

    def test_should_return_file_name_if_no_path_details_exist(self):
        file_name = self.util.get_file_name("DVIM.xml")
        assert file_name == "DVIM.xml"

    def test_should_return_file_name_excluding_extension(self):
        file_name = self.util.get_file_name_excluding_extension("DVIM.xml")
        assert file_name == "DVIM"
