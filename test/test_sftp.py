from unittest import mock
from unittest.mock import ANY, Mock

import pytest

from src.Sftp import Sftp


class TestSftp:
    @mock.patch('pysftp.Connection')
    def test_should_validate_sftp_connection(self, mocked_connection):
        Sftp("host", "user", "password", "dump_zone")
        mocked_connection.assert_called_with(host='host', password='password', username='user', cnopts=ANY)

    @mock.patch("os.path.isfile", return_value=False)
    @mock.patch('pysftp.Connection')
    def test_should_not_upload_file_when_file_not_exist_in_path(self, mocked_connection, mocked_os):
        sftp_object = Sftp("host", "user", "password", "dump_zone")

        with pytest.raises(FileNotFoundError) as error:
            sftp_object.sftp_uploader("file")

        assert not sftp_object.sftp.put.called

    @mock.patch("os.path.isfile", return_value=True)
    @mock.patch('pysftp.Connection')
    def test_should_upload_file(self, mocked_connection, mocked_os):
        sftp_object = Sftp("host", "user", "password", "dump_zone")
        sftp_object.sftp.put = Mock(side_effects={})

        is_uploaded = sftp_object.sftp_uploader("file")

        assert is_uploaded

    @mock.patch("os.path.isfile", return_value=True)
    @mock.patch('pysftp.Connection')
    def test_should_throw_io_error_when_specified_path_not_exist(self, mocked_connection, mocked_os):
        sftp_object = Sftp("host", "user", "password", "invalid_dump_zone")
        sftp_object.sftp.put.side_effect = IOError()

        with pytest.raises(IOError) as io_error:
            sftp_object.sftp_uploader("file_name")

    @mock.patch("os.path.isfile", return_value=True)
    @mock.patch('pysftp.Connection')
    def test_should_throw_error_when_something_failed(self, mocked_connection, mocked_os):
        sftp_object = Sftp("host", "user", "password", "invalid_dump_zone")
        sftp_object.sftp.put.side_effect = Exception()

        with pytest.raises(Exception) as exception:
            sftp_object.sftp_uploader("file_name")

    @mock.patch("os.path.isfile", return_value=True)
    @mock.patch('pysftp.Connection')
    def test_should_return_true_if_file_exist_in_dump_zone(self, mocked_connection, mocked_os):
        sftp_object = Sftp("host", "user", "password", "invalid_dump_zone")
        file1 = Mock()
        file1.filename = "file_name1"
        file2 = Mock()
        file2.filename = "file_name2"
        file3 = Mock()
        file3.filename = "file_name3"
        sftp_object.sftp.listdir_attr.return_value = [file1, file2, file3]

        assert sftp_object.does_file_exists("file_name1")

    @mock.patch("os.path.isfile", return_value=True)
    @mock.patch('pysftp.Connection')
    def test_should_return_false_if_file_not_exist_in_dump_zone(self, mocked_connection, mocked_os):
        sftp_object = Sftp("host", "user", "password", "invalid_dump_zone")
        file1 = Mock()
        file1.filename = "file_name1"
        file2 = Mock()
        file2.filename = "file_name2"
        file3 = Mock()
        file3.filename = "file_name3"
        sftp_object.sftp.listdir_attr.return_value = [file1, file2, file3]

        assert sftp_object.does_file_exists("unknown_file") is False

    @mock.patch('pysftp.Connection')
    def test_should_return_false_if_file_name_is_not_specified(self, mocked_connection):
        sftp_object = Sftp("host", "user", "password", "dump_zone")
        assert sftp_object.does_file_exists(None) is False
        assert sftp_object.does_file_exists("") is False
        assert sftp_object.does_file_exists(" ") is False

    @mock.patch("os.path.isfile", return_value=True)
    @mock.patch('pysftp.Connection')
    def test_should_throw_error_when_file_exist_in_dump_zone(self, mocked_connection, mocked_os):
        sftp_object = Sftp("host", "user", "password", "invalid_dump_zone")
        sftp_object.sftp.listdir_attr.side_effect = Exception

        with pytest.raises(Exception) as exception:
            sftp_object.does_file_exists("file_name")
