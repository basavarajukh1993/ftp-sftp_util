import os
from unittest.mock import patch, Mock, mock_open

import pytest

from src.Ftp import Ftp


class TestFtp:

    @patch('ftplib.FTP', autospec=True)
    def test_should_ftp_connect(self, mock_ftp_constructor):
        mock_ftp = mock_ftp_constructor.return_value

        ftp_object = Ftp("host", "user", "passwd", "dir_name")

        assert ftp_object.ftp == mock_ftp
        assert mock_ftp.login.called is True
        assert mock_ftp_constructor.call_count == 1

    @patch('ftplib.FTP', autospec=True, side_effect=IOError)
    def test_should_throw_error_when_unable_to_connect_to_ftp_host(self, mock_ftp_constructor):
        mock_ftp = mock_ftp_constructor.return_value

        with pytest.raises(IOError) as exception:
            ftp_object = Ftp("host", "user", "passwd", "dir_name")

        assert mock_ftp.login.called is False
        assert mock_ftp_constructor.call_count == 1

    @patch('builtins.open', return_value=Mock())
    @patch('ftplib.FTP', autospec=True)
    def test_should_download_specified_file(self, mocked_ftp, mocked_file_open):
        ftp_object = Ftp("host", "user", "passwd", "dir_name")
        ftp_object.ftp.cwd = Mock(return_value={})
        ftp_object.ftp.retrbinary.side_effect = Mock()

        is_file_downloaded = ftp_object.ftp_downloader("file_name")

        mocked_file_open.assert_called_once()
        assert ftp_object.ftp.retrbinary.called
        assert is_file_downloaded == "/tmp/file_name"

    @patch('builtins.open', return_value=Mock())
    @patch('ftplib.FTP', autospec=True)
    def test_should_throw_error_when_unable_to_read_file(self, mocked_ftp, mocked_file_open):
        ftp_object = Ftp("host", "user", "passwd", "dir_name")
        ftp_object.ftp.cwd = Mock(return_value={})
        ftp_object.ftp.retrbinary.side_effect = IOError()

        with pytest.raises(IOError) as exception:
            ftp_object.ftp_downloader("file_name")

        assert ftp_object.ftp.retrbinary.called

    @patch('builtins.open', return_value=mock_open())
    @patch('ftplib.FTP', autospec=True)
    def test_should_delete_specified_file(self, mocked_ftp, mocked_file_open):
        ftp_object = Ftp("host", "user", "passwd", "dir_name")
        ftp_object.ftp.cwd = Mock(return_value={})
        ftp_object.ftp.delete = Mock()

        is_file_deleted = ftp_object.delete_from_ftp("file_name")

        assert ftp_object.ftp.delete.called
        assert is_file_deleted is True

    @patch('builtins.open', return_value=mock_open())
    @patch('ftplib.FTP', autospec=True)
    def test_should_throw_error_when_unable_to_delete_file(self, mocked_ftp, mocked_file_open):
        ftp_object = Ftp("host", "user", "passwd", "dir_name")
        ftp_object.ftp.cwd = Mock(return_value={})
        ftp_object.ftp.delete.side_effect = IOError

        with pytest.raises(IOError) as exception:
            ftp_object.delete_from_ftp("file_name")

    @patch('builtins.open', return_value=mock_open())
    @patch('ftplib.FTP', autospec=True)
    def test_should_return_true_when_exist_in_drop_zone(self, mocked_ftp, mocked_file_open):
        ftp_object = Ftp("host", "user", "passwd", "dir_name")
        ftp_object.ftp.mlsd = Mock(return_value=['file_name'])

        assert ftp_object.is_file_exist("file_name")

    @patch('builtins.open', return_value=mock_open())
    @patch('ftplib.FTP', autospec=True)
    def test_should_return_false_when_exist_in_drop_zone(self, mocked_ftp, mocked_file_open):
        ftp_object = Ftp("host", "user", "passwd", "dir_name")
        ftp_object.ftp.mlsd = Mock(return_value=[])

        assert ftp_object.is_file_exist("file_name") is False

    @patch('builtins.open', return_value=mock_open())
    @patch('ftplib.FTP', autospec=True)
    def test_should_throw_error_when_some_issue_in_fetching_list(self, mocked_ftp, mocked_file_open):
        ftp_object = Ftp("host", "user", "passwd", "dir_name")
        ftp_object.ftp.mlsd.side_effect = Exception

        with pytest.raises(Exception) as exception:
            ftp_object.is_file_exist("file_name")

    @patch('builtins.open', return_value=mock_open())
    @patch('ftplib.FTP', autospec=True)
    def test_should_upload_file(self, mocked_ftp, mocked_file_open):
        os.environ["STATUS_FILE_NAME"] = "file_name"
        ftp_object = Ftp("host", "user", "passwd", "dir_name")
        ftp_object.ftp.storbinary = Mock(return_value={})

        ftp_object.ftp_upload("file_path")

        assert mocked_file_open.assert_called_once
        assert mocked_file_open.call_args[0] == ("file_path", "rb")
        assert ftp_object.ftp.storbinary.assert_called_once
        assert ftp_object.ftp.storbinary.call_args_list[0][0][0] == 'STOR file_path'

    @patch('builtins.open', side_effect=FileNotFoundError)
    @patch('ftplib.FTP', autospec=True)
    def test_should_throw_error_when_file_not_found_to_upload(self, mocked_ftp, mocked_open):
        os.environ["STATUS_FILE_NAME"] = "file_name"
        ftp_object = Ftp("host", "user", "passwd", "dir_name")

        with pytest.raises(FileNotFoundError) as exception:
            ftp_object.ftp_upload("file_path")
