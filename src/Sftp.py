import logging
import os
import sys
import traceback

import pysftp

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

"""
    Sftp class is used to provide a basic BREAD file operations with the remote SFTP server.

    user: is user name used to login ftp server.
    password: is password used to login ftp server.
    host: is the uri of an ftp server to connect.
    dump_zone: is the directory where files needs to be accessed or stored from/into ftp server. 
"""


class Sftp:
    user: str
    password: str
    host: str
    sftp: pysftp.Connection
    drop_zone: str
    cnopts: None
    no_host_key_check: bool

    '''
        __init__ method called default on object creation, which accepts basic info to connect ftp server.
    '''
    def __init__(
        self, host: str, user: str, password: str, drop_zone: str = "/",
    ) -> None:
        self.host = host
        self.user = user
        self.password = password
        self.drop_zone = drop_zone
        self.cnopts = pysftp.CnOpts()
        self.cnopts.hostkeys = None
        self.sftp = pysftp.Connection(
            host=self.host,
            username=self.user,
            password=self.password,
            cnopts=self.cnopts,
        )

    '''
        sftp_upload used to upload/add a specified file to specified dump-zone of sftp server,
        if specified file does not found in specified file path method raises an file not found error,
        if configured dump-zone not exist in sftp server then method raises an exception.  
    '''
    def sftp_uploader(self, file_path: str) -> bool:
        does_file_exist = os.path.isfile(file_path)
        if not does_file_exist:
            LOGGER.info(f"File {file_path} does not exist in lambda space")
            traceback.print_exception(
                *sys.exc_info(), file=sys.stdout, chain=True,
            )
            raise FileNotFoundError
        else:
            try:
                self.sftp.chdir(self.drop_zone)
                self.sftp.put(file_path)
                LOGGER.info(
                    f"Successfully uploaded file :{file_path} to remote server",
                )
                return True
            except IOError as error:
                LOGGER.error(f"Unable to upload file to remote server: {error}")
                traceback.print_exception(
                    *sys.exc_info(), file=sys.stdout, chain=True,
                )
                raise
            except Exception:
                LOGGER.error(
                    f"Failed to upload file : {file_path} to remote server",
                )
                traceback.print_exception(
                    *sys.exc_info(), file=sys.stdout, chain=True,
                )
                raise

    '''
        does_file_exists used check whether specified file exist in the configured dump-zone of sftp server.  
    '''
    def does_file_exists(self, file_name: str) -> bool:
        if file_name is None or len(file_name.strip()) == 0:
            LOGGER.info("File name is null")
            return False
        try:
            directory_structure = self.sftp.listdir_attr(self.drop_zone)
            for attr in directory_structure:
                if attr.filename == file_name:
                    LOGGER.info(f'Found file: {file_name} "in remote server')
                    return True
            LOGGER.info(f"File: {file_name} does not exist in remote server")
            return False
        except Exception:
            LOGGER.error(
                "Something went wrong while searching for file in remote server",
            )
            traceback.print_exception(
                *sys.exc_info(), file=sys.stdout, chain=True,
            )
            raise

    '''
        __del__ method is destructor method, used to close connections on clearing created objects by garbage collector.
    '''
    def __del__(self):
        LOGGER.info("Closing SFTP connection")
        self.sftp.close()
