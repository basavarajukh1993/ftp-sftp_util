import json
import logging
import os
import sys
import traceback
from json.decoder import JSONDecodeError
from typing import AnyStr

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)
'''
    File class is used for generic file operations, like creating file(with or without content),
    cleaning up the directories, extracting file name with the path and etc., 
'''


class File:

    """
        create_file is used to create a file in a specified path, it create a file with the content if
        any initial content is provided else creates empty path. Method raises an exception in case of file path is not
        correct.
    """
    def create_file(self, file_path: str, content: str):
        try:
            with open(file_path, "w") as file:
                file.write(content)
            LOGGER.info(f"Successfully created new file : {file_path}")
        except Exception:
            LOGGER.error(f"Failed to create file : {file_path}")
            traceback.print_exception(
                *sys.exc_info(), file=sys.stdout, chain=True,
            )
            raise

    """
        clear_tmp_directory is used to clear/delete all the files and directories recursively  from specified path.
    """
    def clear_tmp_directory(self, dir_path: str):
        LOGGER.info(f"Removing files from {dir_path} directory recursively")
        for root, dirs, files in os.walk(dir_path, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        LOGGER.info(f"Successfully removed all files/directories in {dir_path}")

    """
        get_file_name(with extension) is used to extract file name from a given path.
    """
    def get_file_name(self, xml_payload_file_path: str) -> str:
        path_split = os.path.split(xml_payload_file_path)
        return path_split[1]

    """
        get_file_name_excluding_extension is used to extract file name from a given file_name with extension.
    """
    def get_file_name_excluding_extension(self, file_name: str) -> str:
        return file_name.split(".")[0]
