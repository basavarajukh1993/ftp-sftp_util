import ftplib
import logging
import sys
import traceback

try:
    from src.File import File
except ImportError:
    from file import File

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

file_util = File()

"""
    Ftp class is used to provide a basic BREAD file operations with the remote FTP server.
             
    user: is user name used to login ftp server.
    password: is password used to login ftp server.
    host: is the uri of an ftp server to connect.
    local_dump_folder: is the folder where files are accessed or store to, by default
                        it uses '/tmp' folder(specially in case of aws lambda)
    dump_zone: is the directory where files needs to be accessed or stored from/into ftp server. 
"""


class Ftp:
    user: str
    password: str
    host: str
    local_dump_folder = "/tmp/"
    ftp: ftplib.FTP
    dump_zone: str

    '''
        __init__ method called default on object creation, which accepts basic info to connect ftp server
    '''
    def __init__(self, host, user, password, dump_zone="/", local_dump_folder="/tmp/") -> None:
        self.host = host
        self.user = user
        self.password = password
        self.dump_zone = dump_zone
        self.local_dump_folder = local_dump_folder
        self.ftp = self.ftp_connect()

    '''
        ftp_connect method invoked by init method to set up the connection with the provided details, once the 
        authentication is successful method return ftp connection object, otherwise method raises an exception. 
    '''
    def ftp_connect(self) -> ftplib.FTP:
        try:
            ftp = ftplib.FTP(self.host)
            ftp.login(self.user, self.password)
            LOGGER.info("Successfully connected to FTP server")
            return ftp
        except ftplib.all_errors as e:
            LOGGER.error("Error connecting to FTP! Error - {}".format(e))
            traceback.print_exception(
                *sys.exc_info(), file=sys.stdout, chain=True,
            )
            raise

    '''
        ftp_downloader used to download a specified file from specified dump-zone of ftp server and store specified
        local_dump_folder, if not downloads to /tmp folder. In case of file not in specified dump-zone found method 
        raises an exception.
    '''
    def ftp_downloader(self, file_name: str) -> str:
        try:
            LOGGER.info(f"Downloading file: {file_name} from host {self.host}")
            self.ftp.cwd(self.dump_zone)
            file_path = self.local_dump_folder + file_name
            self.ftp.retrbinary(
                "RETR " + file_name, open(file_path, "wb").write,
            )
            LOGGER.info(
                f"Successfully downloaded file: {file_name} and dumped to tmp",
            )
            return file_path
        except ftplib.all_errors as e:
            LOGGER.error(
                "Something went wrong while downloading file".format(e),
            )
            traceback.print_exception(
                *sys.exc_info(), file=sys.stdout, chain=True,
            )
            raise

    '''
        delete_from_ftp used to delete/remove a specified file from specified dump-zone of ftp server,
        if specified file does not found in specified dump-zone method raises an exception. 
    '''
    def delete_from_ftp(self, file_name: str) -> bool:
        try:
            LOGGER.info(f"Deleting file: {file_name} from host {self.host}")
            self.ftp.cwd(self.dump_zone)
            self.ftp.delete(file_name)
            LOGGER.info(f"Successfully deleted file: {file_name}")
            return True
        except ftplib.all_errors as e:
            LOGGER.error("Something went wrong in deleting file ".format(e))
            traceback.print_exception(
                *sys.exc_info(), file=sys.stdout, chain=True,
            )
            raise

    '''
        ftp_upload used to upload/add a specified file to specified dump-zone of ftp server,
        if specified file does not found in specified file path method raises an exception. 
    '''
    def ftp_upload(self, file_path: str):
        try:
            LOGGER.info(f"uploading status file to host {self.host}")
            file = open(file_path, "rb")
            file_name = file_util.get_file_name(file_path)
            ftp_create_command = "STOR " + file_name
            self.ftp.storbinary(ftp_create_command, file)
            LOGGER.info(
                f"Successfully uploaded file: {file_path} to host.",
            )
        except Exception:
            LOGGER.error(f"Failed to upload file: {file_path}")
            traceback.print_exception(
                *sys.exc_info(), file=sys.stdout, chain=True,
            )
            raise

    '''
        is_file_exist used to know whether the file file exist in the dump-zone of an ftp server,
        method returns true when specified files exists, if not returns false.
        In-case of dump-zone not exist in ftp server method raises an exception 
    '''
    def is_file_exist(self, file_name: str) -> bool:
        try:
            for file in self.ftp.mlsd(self.dump_zone + "/" + file_name):
                LOGGER.info(f"Successfully found file : {file_name}")
                return True
            LOGGER.info(
                f"File : {file_name} not exist in drop zone : {self.dump_zone}",
            )
            return False
        except Exception as exception:
            LOGGER.error(
                f"Something went wrong while searching for a file. {exception}",
            )
            traceback.print_exception(
                *sys.exc_info(), file=sys.stdout, chain=True,
            )
            raise

    '''
        __del__ method is destructor method, used to close connections on clearing created objects by garbage collector.
    '''
    def __del__(self):
        self.ftp.close()
